import { Slider } from '@mui/material'
import Grid from '@mui/material/Grid'
import React, { useEffect, useState } from 'react'
import 'react-scrubber/lib/scrubber.css'
import KeyboardEventHandler from 'react-keyboard-event-handler'

interface IImage {
	url: string
	value: string
	description: string
}

function App() {
	const [src, setSrc] = useState<string>('')
	const [value, setValue] = useState<number>(0)
	const [max, setMax] = useState<number>(0)
	const [imgArr, setImgArr] = useState<IImage[]>([])
	const [play, setPlay] = useState<boolean>(false)

	useEffect(() => {
		const response = fetch('data.json', {
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
			},
		}).then((data) => {
			return data.json()
		})

		response.then((data) => {
			setImgArr(data.data)
		})
	}, [setImgArr])

	useEffect(() => {
		if (imgArr.length > 0) {
			setSrc(imgArr[value].url)
			setMax(imgArr.length - 1)
		}
	}, [imgArr, setMax])

	const handleChange = (e: any) => {
		const changedValue = e.target.value
		setValue(changedValue)
		setSrc(imgArr[changedValue].url)
	}

	const handleKeyChange = (key: string) => {
		if (key === 'space') {
			setPlay(!play)
		}
	}

	return (
		<div className='App'>
			<Grid container direction='row' justifyContent='center' alignItems='center' spacing={2}>
				<Grid item xs={8}>
					{imgArr.length > 0 && <img alt='Sample' src={src} />}
					<Slider value={value} aria-label='Temperature' valueLabelDisplay='auto' step={1} marks min={0} max={max} onChange={handleChange} />
					<KeyboardEventHandler handleKeys={['space']} onKeyEvent={handleKeyChange} />
				</Grid>
			</Grid>
		</div>
	)
}

export default App
